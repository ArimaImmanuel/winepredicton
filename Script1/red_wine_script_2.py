from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score, confusion_matrix, accuracy_score, recall_score, precision_score
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn import linear_model
from math import sqrt
import numpy as np
import warnings
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from prettytable import PrettyTable





df = pd.read_csv("/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/winequality-red.csv")

print(df.head(10))

print(df.shape)

df.columns = df.columns.str.replace(' ', '_')

df.info()
df.isnull().sum()


#Calculate and order correlations
correlations = df.corr()['quality'].sort_values(ascending=False)
#print(correlations)

#Feature Selection
print(correlations[abs(correlations) > 0.2])


#Regression Models

#We separe our features from our target feature (quality) and we split data intro training and test
X = df.loc[:,['alcohol','sulphates','citric_acid','volatile_acidity']]
Y = df.iloc[:,11]

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, random_state=42)


#Linear Regression
#Fit the model and make prediction
regressor = LinearRegression()
regressor.fit(X_train, y_train)
y_prediction_lr = regressor.predict(X_test)
y_prediction_lr = np.round(y_prediction_lr)
plt.scatter(y_test,y_prediction_lr)
plt.title("Prediction Using Linear Regression")
plt.xlabel("Real Quality")
plt.ylabel("Predicted")
plt.savefig('/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/Linear_Regression/Prediction_using_Linear_Regression')
plt.show()

#Confussion Matrix for the Linear Regression Model
label_aux = plt.subplot()
cm_linear_regression = confusion_matrix(y_test,y_prediction_lr)
cm_lr = pd.DataFrame(cm_linear_regression,
                     index = ['3','4','5','6','7','8'], 
                     columns = ['3','4','5','6','7','8'])
sns.heatmap(cm_lr,annot=True,fmt="d")
label_aux.set_xlabel('Predicted Quality');label_aux.set_ylabel('True Quality')
plt.savefig('/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/Linear_Regression/Confusion_Matrix_for_Linear_Regression')
plt.show()

#Decision Tree Regressor
#Fit the model and make prediction
regressor = DecisionTreeRegressor()
regressor.fit(X_train, y_train)
y_prediction_dt = regressor.predict(X_test)
y_prediction_dt = np.round(y_prediction_dt)
plt.scatter(y_test,y_prediction_dt)
plt.title("Prediction Using Decision Tree Regression")
plt.xlabel("Real Quality")
plt.ylabel("Predicted")
plt.savefig('/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/Decision_Tree_Regressor/Prediction_using_Decision_Tree_Regressor')
plt.show()

#Confussion Matrix for the Decission Tree Regression Model
label_aux = plt.subplot()
cm_decision_tree_regression = confusion_matrix(y_test,y_prediction_dt)
cm_dt = pd.DataFrame(cm_decision_tree_regression,
                     index = ['3','4','5','6','7','8'], 
                     columns = ['3','4','5','6','7','8'])
sns.heatmap(cm_dt,annot=True,fmt="d")
label_aux.set_xlabel('Predicted Quality');label_aux.set_ylabel('True Quality')
plt.savefig('/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/Decision_Tree_Regressor/Confusion_Matrix_for_Decision_Tree_Regressor')
plt.show()

#Random Forest Regressor
regressor = RandomForestRegressor(n_estimators=10,random_state = 42)
regressor.fit(X_train, y_train)
y_prediction_rf = regressor.predict(X_test)
y_prediction_rf = np.round(y_prediction_rf)
plt.scatter(y_test,y_prediction_rf)
plt.title("Prediction Using Random Forest Regression")
plt.xlabel("Real Quality")
plt.savefig('/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/Random_Forest_Regressor/Prediction_using_Random_Forest_Regressor')
plt.ylabel("Predicted")
plt.show()

#Confussion Matrix for the Random Forest Regression Model
label_aux = plt.subplot()
cm_random_forest_regression = confusion_matrix(y_test,y_prediction_rf)
cm_rf = pd.DataFrame(cm_random_forest_regression,
                     index = ['3','4','5','6','7','8'], 
                     columns = ['3','4','5','6','7','8'])
sns.heatmap(cm_rf,annot=True,fmt="d")
label_aux.set_xlabel('Predicted Quality');label_aux.set_ylabel('True Quality')
plt.savefig('/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/Random_Forest_Regressor/Confusion_Matrix_for_Random_Forest_Regressor')
plt.show()

Regs, Scores = ["Linear Regression", "Decison Tree Regression", "Random Forest Regression"], []*0

#Linear Regression RMSE
LRRMSE = sqrt(mean_squared_error(y_test, y_prediction_lr))
Scores.append(LRRMSE)
#Decision Tree Regressor RMSE
DTRRMSE = sqrt(mean_squared_error(y_test, y_prediction_dt))
Scores.append(DTRRMSE)
#Random Forest Regression RMSE
RFRRMSE = sqrt(mean_squared_error(y_test, y_prediction_rf))
Scores.append(RFRRMSE)

print(LRRMSE,DTRRMSE,RFRRMSE)

matter = ""

for i in range(3):
    
    matter += "The RMSE of {} is --> {}\n\n".format(Regs[i], Scores[i])

f = open("/media/arima/DATA/Arima/PROJECTS/Outbox/YetToBePaid/Red Wine Project/Datasets/DS1/RMSE-Scores.txt", "w+")

f.write(matter)

f.close()



